<?php
include_once "../app/Database.php";
include_once "base_photostudio.php";
  class Photostudio extends Base_photostudio
  {    
    protected $errors=[];
    protected function get_error($attribute)
    {
      return $this->errors[$attribute];
    }
    protected function has_errors()
    {
      $checkvalue=true;
      foreach ($this->errors as $e)
      {
        if (!empty($e))
        {
          $checkvalue=false;
        }
      }
      return $checkvalue;
    }
    public $url="";
    public function set_url($url)
    {
      $this->url=$url;
    }
    public function get_attributes()
    {
      return $this->attributes;
    }
    public function make_search()
    {
      if (!empty($_GET['input_field']))
      {
        $this->show_results();
      }
      echo  '<div class="container ">
              <form class="form-inline" role="search" method="GET" action="index.php?">
                <div class="input-group">
                <div class="input-group-btn"> 
                <input type="text" name="'.$this->name.'" hidden="true"> 
                <select name="choice" class="form-control">';
      foreach ($this->attributes as $a)
      {
        echo '<option name="'.$a.'" ';
        if (!empty($_GET['choice']) && $_GET['choice']==$a)
        {
          echo 'selected>';
        }
        else
        {
          echo '>';
        }
        echo $a.'</option>';
      }
          echo '</select>';
          echo '</div>
                  <input type="text" class="form-control" placeholder="Поиск" name="input_field"
                   value="';
          if (!empty($_GET['input_field'])) 
          {
            echo $_GET['input_field'];
          }
          echo '">
                <div class="input-group-addon ">
                    Точный
                    <input type="checkbox" name="flag" ';
                    if (!empty($_GET['flag']))
                    {
                      echo 'checked>';
                    }
                    else
                    {
                      echo '>';
                    }
          echo '</div>
                <div class="input-group-btn">
                    <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span> Поиск
                    </button>
                  </div>
                </div>
                </form>
              </div><br>';
    }

    public function show_results()
    {
        if (isset($_GET['flag']))
        {
          $array=$this->precise_search();   
        }
        else
        {
          $array=$this->search();       
        }
        echo '<h3>Результаты поиска по полю <i>"'.$_GET['choice'].'"</i>, где значение <i>"'.$_GET['input_field'].'"</i></h3>';
        $this->make_row($array);
        echo '<br><br>';
    }

    public function make_title($table, $field)
    {
      $title="";
      foreach ($this->get_fields($table, $field) as $row)
      {
        foreach (get_attributes($table) as $attribute)
        {
          if ($attribute!='id')
          {
            $title.="[".$attribute."] ".$row[$attribute]."\n";
          }
        }
      }
      return $title;
    }

    public function make_row($array)
    {
      echo '<br><br><table class="table table-striped" border="1" cellspacing="0">';
      echo '<tr>';
      if (isset($_GET['action']))
      {
          switch($_GET['action'])
          {
            case 'reestablish':
              $str= 'action=removed&';
              break;
            case 'delete':
              $str= "";
              break;
            default:
              $str = 'action='.$_GET['action'].'&';
            break;
          }
      }
      else
      {
        $str="";
      }
      foreach ($this->attributes as $attribute)
      {
        echo '<th style="padding: 8px 2px;" class="my">
                    <a class="my" href="/photostudio/admin/index.php?'.$this->name.'&'.$str.'sort=on&field='.$attribute.'" title="Сортировать по возрастанию">↓
                    </a>
                    '.$attribute.'
                      <a class="my" href="/photostudio/admin/index.php?'.$this->name.'&'.$str.'sort=on&field='.$attribute.'&type=desc" title="Сортировать по убыванию">↑
                      </a>
              </th>';
      }
      echo '</tr>';
      $row=0;
      foreach ($array as $value)
      {
        $row++;
        echo '<tr>';
          foreach ($this->attributes as $a)
          {
            if (explode("_", $a)[0]=='id' && $a!='id')
            {
              $str=substr($a, 3);
              echo '<td align=center><a href="#" title ="'.$this->make_title($str, $value[$a]).'">' . $value[$a] . '</a></td> ';
            }
            else
            {
              echo '<td align=center>' . $value[$a] . '</td> ';
            }  
          }
          echo '<td class="my"><a class="my" href="/photostudio/admin/index.php?'.$this->name.'&action=edit&id=' . $value['id'] . '">Изменить</a></td>';
          if ($value['deleted_at']=="0000-00-00 00:00:00")
          {
            $str='<td class="my"><a class="my" href="/photostudio/admin/index.php?'.$this->name.'&action=delete&id=';
            $word='">Удалить</a></td></tr>';
          }
          else
          {
            $str='<td class="my"><a class="my" name="'.$row.'" href="/photostudio/admin/index.php?'.$this->name.'&action=reestablish&id=';
            $word='">Восстановить</a></td></tr>';
          }

          $url = $str.$value['id'];
          if (isset($_GET['sort']))
          { 
            if (isset($_GET['type']))
            {
              echo $url.'&sort=on&field='.$_GET['field'].'&type=desc#'.$row++.$word;
            }
            else
            {
              echo $url.'&sort=on&field='.$_GET['field'].'#'.$row++.$word;          
            }
          }
          else
          {
            echo $url.$word;
          }
        }
      echo '</table>';
    }
    
    public function make_table()
    {
      echo '<a href="/photostudio/admin/index.php?'.$this->name.'&action=add">Добавить новую запись</a>&nbsp';
      if (!empty($_GET['action']) && ($_GET['action']=='removed' or $_GET['action']=='reestablish'))
      { 
        echo '<a href="/photostudio/admin/index.php?'.$this->name.'">Показать существующие</a>';
        $array=$this->get_removed();
      }
      else
      {
        echo '<a href="/photostudio/admin/index.php?'.$this->name.'&action=removed">Показать удаленные</a>';
        $array=$this->get_table();
      }
      $this->make_row($array);
    }
    
    public function make_form($action)
    {
      switch ($action)
      {
        case "edit":
          echo "<label class='label label-info'>Редактировать запись id = ".$_GET['id'].":</label>";
          $value=$this->edit();
          break;
        case "create":
          echo "<h3>Новая запись:</h3>";
          break;
      }
      foreach ($this->attributes as $attribute)
      {
        if ($attribute != 'id' && $attribute != 'created_at')
        {
          echo '<div class="form-group">
                  <label class="col-sm-2 control-label">'.$attribute.'</label>';
            echo '<div class="col-sm-6">';
            if (explode("_", $attribute)[0]=='id')
            {               
              
              echo '<select name="'.$attribute.'" class="form-control">';
                $str=substr($attribute, 3);
                $arr=$this->get_field($str);
                $attr='id';
              foreach ($arr as $key)
              {
                if (isset($_POST[$attribute]) && $key[$attr]==$_POST[$attribute])
                {
                  echo '<option selected name="'.$key[$attr].'">'.$_POST[$attribute].'</option>';
                }
                else if (!empty($value[$attribute]) && $key[$attr]==$value[$attribute])
                {
                  echo '<option selected name="'.$key[$attr].'">'.$value[$attribute].'</option>';
                }    
                else 
                {
                  echo '<option name="'.$key[$attr].'">'.$key[$attr].'</option>';
                }
              }
              echo '</select>';
            }
            else if($attribute=='date')
            {
              echo '<input type="date" class="form-control" name="'.$attribute.'" value="';
              if (isset($_POST[$attribute]))
              {
                echo $_POST[$attribute];
                echo '">' ;
                echo $this->get_error($attribute);
              }
              else if(!empty($value[$attribute]))
              {
                echo $value[$attribute]; 
                echo '">' ; 
              } 
              else
              {
                echo '">';
              }
            }
            else if($attribute=='time')
            {
              echo '<input type="time"  step="1800"  min="08:00:00" max="20:00:00" class="form-control" name="'.$attribute.'" value="';
              if (isset($_POST[$attribute]))
              {
                echo $_POST[$attribute]; 
                echo '">' ; 
                echo $this->get_error($attribute);
              }
              else if(!empty($value[$attribute]))
              {
                echo $value[$attribute];
                echo '">' ; 
              }
              else
              {
                echo '">';
              }
              
            }
            else
            {
              echo '<input type="text" class="form-control" name="'.$attribute.'" value="';
              if(!empty($value[$attribute]))
              {
                echo $value[$attribute]; 
                echo '">';
              }
              else if (isset($_POST[$attribute]))
              {
                echo $_POST[$attribute];
                echo '">';
                echo $this->get_error($attribute);
              }
              else 
              {
                echo '">';
              }
            }
            echo "</div></div>";
        }
      }        
      echo '<div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-success" name="save" value="Сохранить"></div></div>';
      
    }
    protected function validate_name($attribute)
    {
      preg_match("/^[А-Я]{1}[а-я]*/u", $_POST[$attribute], $enter);
      if (empty($enter))
      {
        return false;
      }
      else
      {
        $_POST[$attribute]=$enter[0];
      }
      return true;
    }
    protected function validate_phone()
    {
      preg_match("/(\+7|8)(\d{10}|((\s\d{3}){2})((-\d{2}){2}))/", $_POST['phone'], $enter);
      if (empty($enter))
      {
        return false;
      }
      else
      {
        $_POST['phone']='8'.$enter[2];
      }
     return true;
    }
    protected function validate_passport()
    {
      preg_match("/(\d{4})((\s\d{6})|(\d{6}))/", $_POST['passport'], $enter);
      if (empty($enter))
      {
        return false;
      }
      else
      {
        $_POST['passport']=trim($enter[1])." ".trim($enter[2]);
      }
      return true;
    }

    public function validate($method)
    {
      foreach ($this->attributes as $a)
      {
        if ($a!='name')
        {
          $this->set_errors($a);
        }
        if ($a=='surname')
        {
          $this->set_errors('name');
        }
      }
      if ($this->has_errors())
      {
            
        if ($method=='create')
        {
          $this->insert();
          header('Location: /photostudio/admin/'.$this->name.'.php?action=insert');
        }
        else if ($method=='edit')
        {
          $this->update();
          header('Location: /photostudio/admin/'.$this->name.'.php?action=update');
        }
      }         
    }
    protected function set_errors($attribute)
    {
      if (isset($_POST[$attribute]))
      {
        if ($_POST[$attribute]=="")
        {
          $this->errors[$attribute]='<label class="label label-danger">Вы не ввели поле</label>';
        }
        else
        {
          $this->errors[$attribute]="";
          switch ($attribute)
          {
            case 'name':
            case 'surname':
              if (!$this->validate_name($attribute))
              {
                $this->errors[$attribute]='<label class="label label-danger">Используйте только русские символы и первую заглавную букву</label>';
              }
              else 
              {
                $this->errors[$attribute]= "";
              }
              break;
            case 'phone':
              if (!$this->validate_phone())
              {

                $this->errors[$attribute]='<label class="label label-danger">Неверно:( Поле должно содержать 11 цифр мобильного (с "8" или "+7")</label>';
              }
              else 
              {
                $this->errors[$attribute]= "";
              }
              break;
            case 'passport':
              if (!$this->validate_passport())
              {
                $this->errors[$attribute]='<label class="label label-danger">Неверно:( Введите 10 цифр: серия и номер паспорта</label>';
              }
              else 
              {
                $this->errors[$attribute]= "";
              }
              break;
            default:
              $this->errors[$attribute]= "";
              break;
          }
        }
      }
    }
  }