<?php
include_once "config.php";
  class Base_photostudio
  {
    public $name;
    public $attributes=[];
    function __construct($n)
    {
      $this->name=$n;
      $this->attributes=get_attributes($n);
    }  
    public function get_table()
    {
      $pdo=Database::get_pdo();
      return $pdo->query('SELECT * FROM `'.$this->name.'` WHERE deleted_at="0000-00-00 00:00:00"'.$this->get_str_sort().';');
    }
    
    public function get_field($table)
    {
      $pdo=Database::get_pdo();
      return $pdo->query('SELECT `id` FROM `'.$table.'` WHERE deleted_at="0000-00-00 00:00:00" ORDER BY `id`;');
    } 
    
    public function get_fields($table, $id)
    {
      $pdo=Database::get_pdo();
      return $pdo->query('SELECT * FROM `'.$table.'` WHERE id='.$id.';');
    } 
    
    public function order_by()
    {
      if (!empty($_GET['type']))
      {
        $desc=$_GET['type'];
      }
      else
      {
        $desc="";
      }
      switch ($flag)
      {
        case 'removed':
          $desc+=' '.'WHERE deleted_at="0000-00-00 00:00:00"';
          break;
      }
      $pdo=Database::get_pdo();
      return $pdo->query('SELECT * FROM `'.$this->name.'` ORDER BY `'.$_GET['field'].'` '.$desc.';');     
    }
    public function get_str_sort()
    {
      if (isset($_GET['sort']) && $_GET['sort']=="on")
      {
        if (!empty($_GET['type']))
        {
          $desc=$_GET['type'];
        }
        else
        {
          $desc="";
        }
        return 'ORDER BY `'.$_GET['field'].'` '.$desc;
      }
      else
      {
        return "";
      }
    }
    public static function create_user()
    {
      $attributes=['id', 'username', 'password', 'created_at'];
      $s=[
        ':id'=>NULL,
        ':username'=>$_POST['username'],
        ':password'=>password_hash($_POST['password'], PASSWORD_DEFAULT),
        ':created_at'=>date('Y-m-d h:i:s'),
          ];
      $pdo = Database::get_pdo();
      $sql=$pdo->prepare('INSERT INTO `users` (`'.implode($attributes,"`, `").'`) VALUES (:'
                                                           .implode($attributes,", :").');');
      $sql->execute($s);
    }
    public static function get_all_users()
    {
      $pdo=Database::get_pdo();
      return $pdo->query('SELECT * FROM `users`;');
    }
    public function get_removed()
    {
      $pdo=Database::get_pdo();
      return $pdo->query('SELECT * FROM `'.$this->name.'` WHERE deleted_at!="0000-00-00 00:00:00" '.$this->get_str_sort().';');
    }   
    public function delete_at()
    {
      $pdo=Database::get_pdo();
      $sql = $pdo->prepare('UPDATE`'.$this->name.'` SET `deleted_at`=NOW() WHERE `id` = :id LIMIT 1;');
      $sql->execute([':id' => $_GET['id']]);
    }
    public function reestablish()
    {
      $pdo=Database::get_pdo();
      $sql = $pdo->prepare('UPDATE`'.$this->name.'` SET `deleted_at`="0000-00-00 00:00:00" WHERE `id` = :id LIMIT 1;');
      $sql->execute([':id' => $_GET['id']]);
    } 
    public function edit()
    {
      $pdo=Database::get_pdo();
      $sql=$pdo->prepare('SELECT * FROM `'.$this->name.'` WHERE `id` = :id LIMIT 1;');
      $sql->execute([':id' => $_GET['id']]);
      return $sql->fetch();
    }

    
    public function update()
    {
      $s=[];
      $pdo = Database::get_pdo();
      $str=[];
      foreach ($this->attributes as $a)
      {
        if ($a!='id')
        {
          $s[":".$a] = $_POST[$a];
          $str[$a]=$a."=:".$a;
        }
          
      }
      $s[":id"] = $_GET['id'];
      $s[":deleted_at"] = "0000-00-00 00:00:00";
      
      $sql=$pdo->prepare('UPDATE `'.$this->name.'` SET '.implode($str, ", ").',`deleted_at`=:deleted_at WHERE `id` = :id LIMIT 1;');
      $sql->execute($s);
      
    }
    public function insert()
    {   
      //echo "hello";
      $s=[];
      foreach($this->attributes as $a)
      {
        if ($a=='id')
        {
          $s[":".$a]=NULL;    
        }
        else
        {
          $s[":".$a] = $_POST[$a];
        }
        $s[":deleted_at"]= "0000-00-00 00:00:00";
      }
      $pdo = Database::get_pdo();
      $sql=$pdo->prepare('INSERT INTO `'.$this->name.'` (`'.implode($this->attributes,"`, `").'`, `deleted_at`) VALUES (:'
                                                           .implode($this->attributes,", :").', :deleted_at);');
      $sql->execute($s);
    }
    public function search()
    {
      $pdo = Database::get_pdo();
      return $pdo->query('SELECT * FROM `'.$this->name.'` WHERE '.$_GET['choice'].' LIKE "%'.$_GET['input_field'].'%" '.$this->get_str_sort().';');
    }
    public function precise_search()
    {
      $pdo = Database::get_pdo();
      return $pdo->query('SELECT * FROM `'.$this->name.'` WHERE '.$_GET['choice'].' = "'.$_GET['input_field'].'" '.$this->get_str_sort().';');
    }
  }