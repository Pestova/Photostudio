<?php
include_once "app/Database.php";
include_once "app/model/base_photostudio.php";
  class Login  
  {
    public static $login;
    public static $password;
    public static $exit;
    public static $error='';
	public function verify()
	{
		$users=Base_photostudio::get_all_users();
		foreach ($users as $u)
		{
			if ($_POST['username']==$u['username'] && password_verify($_POST['password'], $u['password']) )
			{
				return true;
			}
		}
		return false;
	}   
    public function set_values()
    {
    	static::$login=isset($_POST['username'])? $_POST['username']: '';
      	static::$password=isset($_POST['password'])? $_POST['password'] : '' ;      
    }
	protected static $start_time;
    public function forget()
    {
       	Session::forget('time');
        return header("Location: ../login.php");   
    }
    public function get_time()
    {
    	return (time()-Session::get("time"));
    }
    public static function delete()
    {
		if (static::get_time() > 300)
      	{
			static::forget();
      	}
    }
    public static function admin_exit($method)
    {
      	if ($_SERVER['REQUEST_METHOD']==="POST")
      	{
    		static::forget();
      	}
    }
  }
