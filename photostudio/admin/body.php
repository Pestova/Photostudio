﻿<body style="background: #fff;">
	<div class="container">
		<p style="text-align: right;"><a href="/photostudio/login.php">Выход <span class="glyphicon glyphicon-log-out"></span></a></p>
	    <h2>ФОТОСТУДИЯ</h2><hr>
	    <div class="row">
	        <div class="col-md-3 ">
	            <ul class="nav nav-pills nav-stacked">
	            	<form action="" method="get">
		            	<input type="submit" class="btn <?= isset($_GET['hall'])||empty(array_keys($_GET)[0]) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='hall' value="Залы">
	            	
	            		<input type="submit" class="btn <?= isset($_GET['event']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='event' value="Мероприятия">    	
	            	
	            		<input type="submit" class="btn <?= isset($_GET['equipment']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='equipment' value="Оборудование">    	
	            	
	            		<input type="submit" class="btn <?= isset($_GET['certificate']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='certificate' value="Сертификаты">    	
	            	
	            		<input type="submit" class="btn <?= isset($_GET['photographer']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='photographer' value="Фотографы">    	
	            	
	            		<input type="submit" class="btn <?= isset($_GET['shedule_photographer']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='shedule_photographer' value="График работы фотографов">    	
	            	
	            		<input type="submit" class="btn <?= isset($_GET['order']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='order' value="Заказы">  

	            		<input type="submit" class="btn <?= isset($_GET['client']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='client' value="Заказчики">  

	            		<input type="submit" class="btn <?= isset($_GET['ordering_the_certificate']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='ordering_the_certificate' value="Заказанные сертификаты">    	
	            	
	            		<input type="submit" class="btn <?= isset($_GET['rent_of_equipment']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='rent_of_equipment' value="Аренда оборудования">
	            		<input type="submit" class="btn <?= isset($_GET['duration_of_photo_shoot']) ? "btn-primary" : "btn-link"?> btn-block btn-left" name='duration_of_photo_shoot' value="Длительность фотосессий">    	
	            	</form>	            
	            </ul>
	        </div>
	        <div class="col-md-9 well">
	          <?php
		          if (empty(array_keys($_GET)[0]))
		          {
		          	main_table('hall');
		          }
		          else
		          {
		          	main_table(array_keys($_GET)[0]);
		          }
	          ?>
	        </div>
    	</div>
	</div>
</body>