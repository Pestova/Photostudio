<?php
	include_once "../app/Session.php";
	include_once "../app/model/input.php";
	if (empty(Session::get('username')))
	{
		header('Location: ../login.php');
	}  
	else
	{
		include_once "header.php";
	    include_once "body.php";
	}    
		
?>