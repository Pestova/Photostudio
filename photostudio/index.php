﻿<html>
<head>
  <title>Фотостудия</title>
  <link href="css/style.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="login-container">
            <h2>ФОТОСТУДИЯ</h2>
            <div class="form-box">   
	            <span class="group-btn">     
	                <a href="login.php" class="btn btn-primary btn-md">Войти как администратор <span class="glyphicon glyphicon-log-in"></span></a>
	            </span><br>
	            <span class="group-btn">     
	                <a href="user/index.php" class="btn btn-primary btn-md">Сделать заказ <span class="glyphicon glyphicon-pencil"></span></a>
	            </span>
        	</div>
    	</div>
	</div>
</div>
</body>
</html>