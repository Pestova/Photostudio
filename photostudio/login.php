<html>
<head>
  <title>Фотостудия</title>
  <link href="css/style.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <?php
    include_once "app/model/input.php";
    include_once "app/Session.php";
  ?>
</head>
<body>
<div class="container">
<label class="label label-info">Авторизация предложена работникам фотостудии, если у вас нет пароля, возьмите его у начальства!:)</label>

	<div class="login-container">
		<h2>Авторизация</h2>        	

        <div class="form-box">
        	
            <form action="" method="post">

            	<?php 
            		if ((!empty($_POST['username']) && !empty($_POST['password'])) || !empty(Session::get('username')))
            		{
            			if (!Login::verify() && empty(Session::get('username')))
	            		{
	            			echo '<label class="label-danger">Неправильный логин или пароль!:( Попробуйте еще раз.</label>';
	            		}
	            		else 
	            		{
                    if (empty(Session::get('username')))
                    {
                      Session::set('username', $_POST['username']);
                      Session::set("time", time());
                      header('Location: admin/index.php');
                    }
	            			else
                    {
                      Session::forget('username');
                      Session::forget("time");
                    }
	            		}
            		}	
            	?>
                <input name="username" type="text" placeholder="username">
                <input type="password" name="password" placeholder="password">
                <button class="btn btn-info btn-block login" type="submit">Войти <span class="glyphicon glyphicon-log-in"></span></button>
                <span class="group-btn">     
	                <a href="index.php" class="btn btn-primary btn-block login">Вернуться <span class="glyphicon glyphicon-arrow-left"></span></a>
	            </span>
            </form>
        </div>
    </div> 

</div>
</body>
</html>