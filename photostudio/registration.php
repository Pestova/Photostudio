<html>
<head>
  <title>Фотостудия</title>
  <link href="css/style.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <?php 
    include_once "app/Database.php";
    include_once "app/model/base_photostudio.php";
  ?>
</head>
<body>
<div class="container">
  <div class="login-container">
      <h2>Регистрация</h2>
      <div id="output"></div>
      <div class="form-box">
          <form action="#" method="post">
            <?php
              if (!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['password2']))
              {
                Base_photostudio::create_user();
                echo '<label class="success">Успешно!';
                echo '<a href="login.php" class="btn btn-success btn-md">Авторизоваться<span class="glyphicon glyphicon-log-in"></span></a>';
              }
              else
              {
                echo '<input name="username" type="text" placeholder="username">
                      <input type="password" name="password" placeholder="new password">
                      <input type="password" name="password2" placeholder="repeat password">
                      <button class="btn btn-info btn-block login" type="submit">Зарегистрироваться</button>';
              }
            ?>
              
          </form>
      </div>
  </div> 
  </div>
</div>
</body>
</html>